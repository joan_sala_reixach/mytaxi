package com.mytaxi.domainvalue;

public enum SearchField {
    USERNAME,
    ONLINE_STATUS,
    LISCENSE_PLATE,
    SEAT_COUNT,
    CONVERTIBLE,
    RATING,
    ENGINE_TYPE
}
