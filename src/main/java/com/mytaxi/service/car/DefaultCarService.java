package com.mytaxi.service.car;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.search.SearchTerm;
import com.mytaxi.domainvalue.SearchField;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DefaultCarService implements CarService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCarService.class);

    private final CarRepository carRepository;

    public DefaultCarService(final CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public CarDO create(CarDO carDO) throws ConstraintsViolationException {
        CarDO car;
        try {
            car = carRepository.save(carDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("ConstraintsViolationException while creating a car: {}", carDO, e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return car;
    }

    @Override
    @Transactional
    public void delete(Long carId) throws EntityNotFoundException {
        CarDO carDO = findCarChecked(carId);
        carDO.setDeleted(true);
    }

    @Override
    public CarDO find(Long carId) throws EntityNotFoundException {
        return findCarChecked(carId);
    }

    private CarDO findCarChecked(Long carId) throws EntityNotFoundException {
        return carRepository.findById(carId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + carId));
    }

    @Transactional
    public void assignDriver(Long carId, Long driverId) throws CarAlreadyInUseException, EntityNotFoundException {
        CarDO car = find(carId);
        if (!car.isInUse()) {
            car.setAssignedDriver(driverId);
        } else {
            throw new CarAlreadyInUseException("Car already in use!");
        }
    }

    @Transactional
    public void unassignDriver(Long carId) throws EntityNotFoundException {
        CarDO car = find(carId);
        car.setAssignedDriver(null);
    }

    @Transactional
    public void updateCar(Long carId, CarDO carDO) throws EntityNotFoundException {
        CarDO car = find(carId);
        car = car.updateCar(carDO);
    }

    public Set<CarDO> findCars(Map<SearchField, SearchTerm> searchTerms) {
        Set<CarDO> cars = new HashSet<>();
        carRepository.findAll().forEach(cars::add);
        if (searchTerms.containsKey(SearchField.LISCENSE_PLATE)) {
            cars = cars.stream()
                    .filter(car -> searchTerms.get(SearchField.LISCENSE_PLATE).matchesTerm(car.getLiscensePlate()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.SEAT_COUNT)) {
            cars = cars.stream()
                    .filter(car -> searchTerms.get(SearchField.SEAT_COUNT).matchesTerm(car.getSeatCount()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.CONVERTIBLE)) {
            cars = cars.stream()
                    .filter(car -> searchTerms.get(SearchField.SEAT_COUNT).matchesTerm(car.getConvertible()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.RATING)) {
            cars = cars.stream()
                    .filter(car -> searchTerms.get(SearchField.RATING).matchesTerm(car.getRating()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.ENGINE_TYPE)) {
            cars = cars.stream()
                    .filter(car -> searchTerms.get(SearchField.ENGINE_TYPE).matchesTerm(car.getEngineType()))
                    .collect(Collectors.toSet());
        }
        return cars;
    }
}
