package com.mytaxi.service.car;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.search.SearchTerm;
import com.mytaxi.domainvalue.SearchField;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.Map;
import java.util.Set;

public interface CarService {

    CarDO create(CarDO carDO) throws ConstraintsViolationException;

    CarDO find(Long driverId) throws EntityNotFoundException;

    void delete(Long carId) throws EntityNotFoundException;

    void assignDriver(Long carId, Long driverId) throws CarAlreadyInUseException, EntityNotFoundException;

    void unassignDriver(Long carId) throws EntityNotFoundException;

    void updateCar(Long carId, CarDO carDO) throws EntityNotFoundException;

    Set<CarDO> findCars(Map<SearchField, SearchTerm> searchTerms);

}
