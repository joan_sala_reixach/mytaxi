package com.mytaxi.service.search;

import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.search.SearchDO;
import com.mytaxi.service.car.CarService;
import com.mytaxi.service.driver.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DefaultSearchService implements SearchService {

    private DriverService driverService;
    private CarService carService;

    @Autowired
    public DefaultSearchService(final DriverService driverService, final CarService carService) {
        this.driverService = driverService;
        this.carService = carService;
    }

    public Set<DriverDTO> searchDrivers(SearchDO searchDO) {
        Set<CarDO> cars = carService.findCars(searchDO.getCarSearchTerms());
        Set<DriverDO> drivers = driverService.findDrivers(searchDO.getDriverSearchTerms(),
                cars.stream().map(car -> car.getCarId()).collect(Collectors.toSet()));
        return drivers.stream()
                .map(driver -> DriverMapper.makeDriverDTO(driver))
                .collect(Collectors.toSet());
    }
}
