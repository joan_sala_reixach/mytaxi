package com.mytaxi.service.search;

import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.search.SearchDO;

import java.util.Set;

public interface SearchService {

    Set<DriverDTO> searchDrivers(SearchDO searchDO);
}
