package com.mytaxi.service.driver;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.search.SearchTerm;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.domainvalue.SearchField;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DriverService
{

    DriverDO find(Long driverId) throws EntityNotFoundException;

    DriverDO create(DriverDO driverDO) throws ConstraintsViolationException;

    void delete(Long driverId) throws EntityNotFoundException;

    void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException;

    List<DriverDO> find(OnlineStatus onlineStatus);

    DriverDO find(String username);

    void selectCar(long driverId, long carId) throws EntityNotFoundException, CarAlreadyInUseException;

    void deselectCar(long driverId) throws EntityNotFoundException;

    Set<DriverDO> findDrivers(Map<SearchField, SearchTerm> searchTerms, Set<Long> carDrivers);

}
