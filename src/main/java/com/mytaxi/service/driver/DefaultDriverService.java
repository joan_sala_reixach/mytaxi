package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.search.SearchTerm;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.domainvalue.SearchField;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.car.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private final DriverRepository driverRepository;

    private final CarService carService;


    public DefaultDriverService(final DriverRepository driverRepository, final CarService carService)
    {
        this.driverRepository = driverRepository;
        this.carService = carService;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException
    {
        return findDriverChecked(driverId);
    }


    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException
    {
        DriverDO driver;
        try
        {
            driver = driverRepository.save(driverDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("ConstraintsViolationException while creating a driver: {}", driverDO, e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus)
    {
        return driverRepository.findByOnlineStatus(onlineStatus);
    }

    @Override
    public DriverDO find(String username) {
        return driverRepository.findByUsername(username);
    }


    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException
    {
        return driverRepository.findById(driverId)
            .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + driverId));
    }

    @Transactional
    public void selectCar(long driverId, long carId) throws EntityNotFoundException, CarAlreadyInUseException {
        DriverDO driver = findDriverChecked(driverId);
        carService.assignDriver(carId, driverId);
        driver.setAssignedCarId(carId);
    }

    @Transactional
    public void deselectCar(long driverId) throws EntityNotFoundException {
        DriverDO driver = findDriverChecked(driverId);
        carService.unassignDriver(driver.getAssignedCarId());
        driver.setAssignedCarId(null);
    }

    public Set<DriverDO> findDrivers(Map<SearchField, SearchTerm> searchTerms, Set<Long> carDrivers) {
        Set<DriverDO> drivers = new HashSet<>();
        driverRepository.findAll().forEach(drivers::add);
        if (carDrivers.size() > 0) {
            drivers = drivers.stream()
                    .filter(driver -> driver.getAssignedCarId() != null && carDrivers.contains(driver.getAssignedCarId()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.USERNAME)) {
            drivers = drivers.stream()
                    .filter(driver -> searchTerms.get(SearchField.USERNAME).matchesTerm(driver.getUsername()))
                    .collect(Collectors.toSet());
        }
        if (searchTerms.containsKey(SearchField.ONLINE_STATUS)) {
            drivers = drivers.stream()
                    .filter(driver -> searchTerms.get(SearchField.ONLINE_STATUS).matchesTerm(driver.getOnlineStatus()))
                    .collect(Collectors.toSet());
        }
        return drivers;
    }
}
