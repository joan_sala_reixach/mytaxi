package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mytaxi.domainvalue.EngineType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDTO {

    private Long carId;
    private String liscensePlate;
    private Integer seatCount;
    private Boolean convertible;
    private Double rating;
    private EngineType engineType;
    
    private CarDTO() {}

    public CarDTO(Long carId, String liscensePlate, Integer seatCount, Boolean convertible, Double rating, EngineType engineType) {
        this.carId = carId;
        this.liscensePlate = liscensePlate;
        this.seatCount = seatCount;
        this.convertible = convertible;
        this.rating = rating;
        this.engineType = engineType;
    }

    public Long getCarId() {
        return carId;
    }

    public String getLiscensePlate() {
        return liscensePlate;
    }

    public Integer getSeatCount() {
        return seatCount;
    }

    public Boolean isConvertible() {
        return convertible;
    }

    public Double getRating() {
        return rating;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public static CarDTOBuilder getNewBuilder() { return new CarDTOBuilder(); }

    public static class CarDTOBuilder
    {
        private Long carId;
        private String liscensePlate;
        private Integer seatCount;
        private Boolean convertible;
        private Double rating;
        private EngineType engineType;

        protected CarDTOBuilder() {}

        public CarDTOBuilder setCarId(Long carId)
        {
            this.carId = carId;
            return this;
        }

        public CarDTOBuilder setLiscensePlate(String liscensePlate)
        {
            this.liscensePlate = liscensePlate;
            return this;
        }

        public CarDTOBuilder setSeatCount(Integer seatCount)
        {
            this.seatCount = seatCount;
            return this;
        }

        public CarDTOBuilder setConvertible(Boolean convertible)
        {
            this.convertible = convertible;
            return this;
        }

        public CarDTOBuilder setRating(Double rating) {
            this.rating = rating;
            return this;
        }

        public CarDTOBuilder setEngineType(EngineType engineType) {
            this.engineType = engineType;
            return this;
        }

        public CarDTO build()
        {
            return new CarDTO(carId, liscensePlate, seatCount, convertible, rating, engineType);
        }

    }
}
