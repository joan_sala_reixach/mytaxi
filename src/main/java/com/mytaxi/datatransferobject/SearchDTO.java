package com.mytaxi.datatransferobject;

import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.OnlineStatus;

public class SearchDTO {

    private String username;
    private OnlineStatus onlineStatus;

    private String liscensePlate;
    private Integer seatCount;
    private Boolean convertible;
    private Double rating;
    private EngineType engineType;

    public String getUsername() {
        return username;
    }

    public OnlineStatus getOnlineStatus() {
        return onlineStatus;
    }

    public String getLiscensePlate() {
        return liscensePlate;
    }

    public Integer getSeatCount() {
        return seatCount;
    }

    public Boolean getConvertible() {
        return convertible;
    }

    public Double getRating() {
        return rating;
    }

    public EngineType getEngineType() {
        return engineType;
    }
}
