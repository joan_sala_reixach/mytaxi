package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.EngineType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(
        name = "car",
        uniqueConstraints = @UniqueConstraint(name = "uc_liscense_plate", columnNames = {"liscensePlate"})
)
public class CarDO {

    @Id
    @GeneratedValue
    private Long carId;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime createDate;

    @Column(nullable = false)
    @NotNull(message = "Liscense plate can not be null!")
    private String liscensePlate;

    @Column
    private Integer seatCount;

    @Column
    private Boolean convertible;

    @Column
    private Double rating;

    @Enumerated(EnumType.STRING)
    @Column
    private EngineType engineType;

    @Column(nullable = false)
    private Boolean deleted = false;

    private Long assignedDriver;

    @Column
    private ManufacturerDO manufacturer;

    private CarDO() {}

    public CarDO(ZonedDateTime createDate, String liscensePlate, Integer seatCount, Boolean convertible, Double rating, EngineType engineType) {
        this.createDate = createDate;
        this.liscensePlate = liscensePlate;
        this.seatCount = seatCount;
        this.convertible = convertible;
        this.rating = rating;
        this.engineType = engineType;
        this.deleted = false;
        this.assignedDriver = null;
    }

    public CarDO(CarDO carDO) {
        this.carId = carDO.carId;
        this.createDate = carDO.createDate;
        this.liscensePlate = carDO.liscensePlate;
        this.seatCount = carDO.seatCount;
        this.convertible = carDO.convertible;
        this.rating = carDO.rating;
        this.engineType = carDO.engineType;
        this.deleted = carDO.deleted;
        this.assignedDriver = carDO.assignedDriver;
    }

    public CarDO updateCar(CarDO carDO) {
        this.liscensePlate = carDO.liscensePlate != null ? carDO.liscensePlate : this.liscensePlate;
        this.seatCount = carDO.seatCount != null ? carDO.seatCount : this.seatCount;
        this.convertible = carDO.convertible != null ? carDO.convertible : this.convertible;
        this.rating = carDO.rating != null ? carDO.rating : this.rating;
        this.engineType = carDO.engineType != null ? carDO.engineType : this.engineType;
        return this;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public String getLiscensePlate() {
        return liscensePlate;
    }

    public void setLiscensePlate(String liscensePlate) {
        this.liscensePlate = liscensePlate;
    }

    public Integer getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(Integer seatCount) {
        this.seatCount = seatCount;
    }

    public Boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(Boolean convertible) {
        this.convertible = convertible;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getAssignedDriver() {
        return assignedDriver;
    }

    public void setAssignedDriver(Long assignedDriver) {
        this.assignedDriver = assignedDriver;
    }

    public Boolean isInUse() {
        return assignedDriver != null;
    }
}
