package com.mytaxi.domainobject.search;

import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.OnlineStatus;

public class StringSearchTerm implements SearchTerm {

    private final String searchString;

    public StringSearchTerm(String searchString) {
        this.searchString = searchString;
    }

    public StringSearchTerm(OnlineStatus onlineStatus) {
        this.searchString = onlineStatus.toString();
    }

    public StringSearchTerm(EngineType engineType) {
        this.searchString = engineType.toString();
    }

    @Override
    public boolean matchesTerm(Object value) {
        return searchString.equals(String.valueOf(value));
    }
}
