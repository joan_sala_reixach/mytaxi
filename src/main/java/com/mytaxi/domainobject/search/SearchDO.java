package com.mytaxi.domainobject.search;

import com.mytaxi.domainvalue.SearchField;

import java.util.HashMap;
import java.util.Map;

public class SearchDO {

    Map<SearchField, SearchTerm> carSearchTerms;
    Map<SearchField, SearchTerm> driverSearchTerms;

    public SearchDO() {
        carSearchTerms = new HashMap<>();
        driverSearchTerms = new HashMap<>();
    }

    public SearchDO(SearchDO searchDO) {
        this.carSearchTerms = searchDO.carSearchTerms;
        this.driverSearchTerms = searchDO.driverSearchTerms;
    }

    public void addCarSearchTerm(SearchField field, SearchTerm term) {
        carSearchTerms.put(field, term);
    }

    public void addDriverSearchTerm(SearchField field, SearchTerm term) {
        driverSearchTerms.put(field, term);
    }

    public Map<SearchField, SearchTerm> getCarSearchTerms() {
        return carSearchTerms;
    }

    public Map<SearchField, SearchTerm> getDriverSearchTerms() {
        return driverSearchTerms;
    }
}
