package com.mytaxi.domainobject.search;

public class BooleanSearchTerm implements SearchTerm {

    private final Boolean searchBoolean;

    public BooleanSearchTerm(Boolean searchBoolean) {
        this.searchBoolean = searchBoolean;
    }

    @Override
    public boolean matchesTerm(Object value) {
        return searchBoolean.equals(Boolean.valueOf(String.valueOf(value)));
    }
}
