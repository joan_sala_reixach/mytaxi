package com.mytaxi.domainobject.search;

public interface SearchTerm {

    boolean matchesTerm(Object value);
}
