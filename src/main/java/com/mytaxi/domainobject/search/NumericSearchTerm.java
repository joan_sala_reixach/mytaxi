package com.mytaxi.domainobject.search;

public class NumericSearchTerm implements SearchTerm {

    private final Double searchNumber;

    public NumericSearchTerm(Double searchNumber) {
        this.searchNumber = searchNumber;
    }

    public NumericSearchTerm(Integer searchNumber) {
        this.searchNumber = Double.valueOf(searchNumber);
    }

    @Override
    public boolean matchesTerm(Object value) {
        return searchNumber.compareTo(Double.valueOf(String.valueOf(value))) <= 0;
    }
}
