package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.SearchDTO;
import com.mytaxi.domainobject.search.BooleanSearchTerm;
import com.mytaxi.domainobject.search.NumericSearchTerm;
import com.mytaxi.domainobject.search.SearchDO;
import com.mytaxi.domainobject.search.StringSearchTerm;
import com.mytaxi.domainvalue.SearchField;

public class SearchMapper {

    public static SearchDO makeSearchDO(SearchDTO searchDTO) {
        SearchDO searchDO = new SearchDO();
        if (searchDTO.getUsername() != null) {
            searchDO.addDriverSearchTerm(SearchField.USERNAME, new StringSearchTerm(searchDTO.getUsername()));
        }
        if (searchDTO.getOnlineStatus() != null) {
            searchDO.addDriverSearchTerm(SearchField.ONLINE_STATUS, new StringSearchTerm(searchDTO.getOnlineStatus()));
        }
        if (searchDTO.getLiscensePlate() != null) {
            searchDO.addCarSearchTerm(SearchField.LISCENSE_PLATE, new StringSearchTerm(searchDTO.getLiscensePlate()));
        }
        if (searchDTO.getSeatCount() != null) {
            searchDO.addCarSearchTerm(SearchField.SEAT_COUNT, new NumericSearchTerm(searchDTO.getSeatCount()));
        }
        if (searchDTO.getRating() != null) {
            searchDO.addCarSearchTerm(SearchField.RATING, new NumericSearchTerm(searchDTO.getRating()));
        }
        if (searchDTO.getConvertible() != null) {
            searchDO.addCarSearchTerm(SearchField.CONVERTIBLE, new BooleanSearchTerm(searchDTO.getConvertible()));
        }
        if (searchDTO.getEngineType() != null) {
            searchDO.addCarSearchTerm(SearchField.ENGINE_TYPE, new StringSearchTerm(searchDTO.getEngineType()));
        }
        return searchDO;
    }
}
