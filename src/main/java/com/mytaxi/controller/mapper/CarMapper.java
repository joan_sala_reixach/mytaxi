package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;

import java.time.ZonedDateTime;

public class CarMapper {

    public static CarDO makeCarDO(CarDTO carDTO) {
        return new CarDO(ZonedDateTime.now(), carDTO.getLiscensePlate(),
                carDTO.getSeatCount(), carDTO.isConvertible(), carDTO.getRating(), carDTO.getEngineType());
    }

    public static CarDTO makeCarDTO(CarDO carDO) {
        CarDTO.CarDTOBuilder carDTOBuilder = CarDTO.getNewBuilder()
                .setCarId(carDO.getCarId())
                .setLiscensePlate(carDO.getLiscensePlate())
                .setSeatCount(carDO.getSeatCount())
                .setConvertible(carDO.getConvertible())
                .setRating(carDO.getRating())
                .setEngineType(carDO.getEngineType());
        return carDTOBuilder.build();
    }
}
