package com.mytaxi.controller;

import com.mytaxi.controller.mapper.SearchMapper;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.datatransferobject.SearchDTO;
import com.mytaxi.service.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("v1/search")
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(final SearchService searchService)
    {
        this.searchService = searchService;
    }


    @RequestMapping
    public Set<DriverDTO> getDriver(@Valid @RequestBody SearchDTO searchDTO)
    {
        return searchService.searchDrivers(SearchMapper.makeSearchDO(searchDTO));
    }
}
