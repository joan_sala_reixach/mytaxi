/**
 * CREATE Script for init of DB
 */

-- Create 2 cars in use

insert into car (car_id, create_date, liscense_plate, seat_count, convertible, rating, engine_type, deleted, assigned_driver)
values
 (109, now(), 'PL-123', 4, false, 4.3, 'HYBRID', false, 104);

insert into car (car_id, create_date, liscense_plate, seat_count, convertible, rating, engine_type, deleted, assigned_driver)
 values
  (110, now(), 'PL-456', 7, false, 3.2, 'GAS', false, 105);

-- Create 1 free car

insert into car (car_id, create_date, liscense_plate, seat_count, convertible, rating, engine_type, deleted, assigned_driver)
values
 (111, now(), 'PL-789', 4, false, 4.3, 'HYBRID', false, null);

-- Create 3 OFFLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (101, now(), false, 'OFFLINE',
'driver01pw', 'driver01');

insert into driver (id, date_created, deleted, online_status, password, username) values (102, now(), false, 'OFFLINE',
'driver02pw', 'driver02');

insert into driver (id, date_created, deleted, online_status, password, username) values (103, now(), false, 'OFFLINE',
'driver03pw', 'driver03');


-- Create 3 ONLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username, assigned_car_id) values (104, now(), false, 'ONLINE',
'driver04pw', 'driver04', 109);

insert into driver (id, date_created, deleted, online_status, password, username, assigned_car_id) values (105, now(), false, 'ONLINE',
'driver05pw', 'driver05', 110);

insert into driver (id, date_created, deleted, online_status, password, username) values (106, now(), false, 'ONLINE',
'driver06pw', 'driver06');

-- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (107,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
'driver07pw', 'driver07');

-- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (108,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
'driver08pw', 'driver08');

